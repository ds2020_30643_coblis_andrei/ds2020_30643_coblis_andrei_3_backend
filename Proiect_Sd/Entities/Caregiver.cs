﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.Entities
{
    public class Caregiver
    {
        [Key]
        public int CaregiverId { get; set; }

        [Required]
        public int UserId { get; set; }

        // Used for EntityFramework
        public User User { get; set; }

        public ICollection<Patient> Patients { get; set; }
    }
}
