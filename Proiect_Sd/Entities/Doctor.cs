﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.Entities
{
    public class Doctor
    {
        [Key]
        public int DoctorId { get; set; }

        [Required]
        public int UserId { get; set; }

        // Used for EntityFramework
        public User User { get; set; }
    }
}
