﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.Entities
{
    public class MedicationPlan
    {
        [Key]
        public int PlanId { get; set; }

        [Required]
        public int PatientId { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        // Used for EntityFramework
        public Patient Patient { get; set; }

        public ICollection<Medication> Medications { get; set; }
    }
}
