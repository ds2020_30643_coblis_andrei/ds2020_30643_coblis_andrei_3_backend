﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Data;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Proiect_Sd.Repositories
{
    public class MedicationPlanRepository : IMedicationPlanRepository
    {
        private readonly DataContext _context;

        public MedicationPlanRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfMedicationPlanExists(int medicationPlanId)
        {
            using(_context)
            {
                return await _context.MedicationPlans.AnyAsync(mp => mp.PlanId == medicationPlanId);
            }
        }

        public async Task<MedicationPlan> CreateMedicationPlan(MedicationPlan medicationPlan)
        {
            using(_context)
            {
                _context.MedicationPlans.Add(medicationPlan);
                await _context.SaveChangesAsync();

                return medicationPlan;
            }
        }

        public async Task<bool> DeleteMedicationPlan(MedicationPlan medicationPlan)
        {
            using(_context)
            {
                _context.MedicationPlans.Remove(medicationPlan);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<MedicationPlan> GetMedicationPlanById(int medicationPlanId)
        {
            using(_context)
            {
                return await _context.MedicationPlans.FirstOrDefaultAsync(m => m.PlanId == medicationPlanId);
            }
        }

        public async Task<MedicationPlanResponseDTO> GetMedicationPlanResponseById(int medicationPlanId)
        {
            using (_context)
            {
                var medicationPlans = _context.MedicationPlans;
                var patients = _context.Patients;

                var medicationPlanResponseDTO = from medicationPlan in medicationPlans
                                                    join patient in patients
                                                    on medicationPlan.PatientId equals patient.PatientId
                                                    where medicationPlan.PlanId == medicationPlanId
                                                    select new MedicationPlanResponseDTO()
                                                    {
                                                        Id = medicationPlan.PlanId,
                                                        PatientName = patient.User.Username,
                                                        StartDate = medicationPlan.StartDate,
                                                        EndDate = medicationPlan.EndDate,
                                                        PatientId = patient.PatientId
                                                    };

                return await medicationPlanResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<MedicationPlanResponseDTO>> GetMedicationPlans()
        {
            using(_context)
            {
                var medicationPlans = _context.MedicationPlans;
                var patients = _context.Patients;

                var medicationPlanResponseDTOList = from medicationPlan in medicationPlans
                                                    join patient in patients
                                                    on medicationPlan.PatientId equals patient.PatientId
                                                    select new MedicationPlanResponseDTO()
                                                    {
                                                        Id = medicationPlan.PlanId,
                                                        PatientName = patient.User.Username,
                                                        StartDate = medicationPlan.StartDate,
                                                        EndDate = medicationPlan.EndDate,
                                                        PatientId = patient.PatientId
                                                    };

                return await medicationPlanResponseDTOList.ToListAsync();
            }
        }

        public async Task<bool> UpdateMedicationPlan(MedicationPlan medicationPlan)
        {
            using(_context)
            {
                _context.MedicationPlans.Update(medicationPlan);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
