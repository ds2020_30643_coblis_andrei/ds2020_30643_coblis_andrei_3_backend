﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Data;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Proiect_Sd.Repositories
{
    public class PatientRepository : IPatientRepository
    {
        private readonly DataContext _context;

        public PatientRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckIfPatientExists(int patientId)
        {
            using(_context)
            {
                return await _context.Patients.AnyAsync(p => p.PatientId == patientId);
            }
        }

        public async Task<Patient> CreatePatient(Patient patient)
        {
            using(_context)
            {
                _context.Patients.Add(patient);
                await _context.SaveChangesAsync();

                return patient;
            }
        }

        public async Task<bool> DeletePatient(Patient patient)
        {
            using(_context)
            {
                _context.Patients.Remove(patient);
                var deleted = await _context.SaveChangesAsync();

                return deleted > 0;
            }
        }

        public async Task<Patient> GetPatientById(int pacientId)
        {
            using(_context)
            {
                return await _context.Patients.FirstOrDefaultAsync(p => p.PatientId == pacientId);
            }
        }

        public async Task<PatientResponseDTO> GetPatientResponseById(int patientId)
        {
            using(_context)
            {
                var patients = _context.Patients;
                var users = _context.Users;
                var caregivers = _context.Caregivers;

                var patientResponseDTO = from patient in patients
                                         join user in users
                                         on patient.UserId equals user.UserId
                                         join caregiver in caregivers
                                         on patient.CaregiverId equals caregiver.CaregiverId
                                         where patient.PatientId == patientId
                                         select new PatientResponseDTO()
                                         {
                                             Username = user.Username,
                                             Id = patient.PatientId,
                                             MedicalRecord = patient.MedicalRecord,
                                             CaregiverName = caregiver.User.Username,
                                             UserId = patient.UserId,
                                             CaregiverId = caregiver.CaregiverId
                                         };

                return await patientResponseDTO.FirstOrDefaultAsync();
            }
        }

        public async Task<List<PatientResponseDTO>> GetPatients()
        {
            using(_context)
            {
                var patients = _context.Patients;
                var users = _context.Users;
                var caregivers = _context.Caregivers;

                var patientResponseDTOList = from patient in patients
                                         join user in users
                                         on patient.UserId equals user.UserId
                                         join caregiver in caregivers
                                         on patient.CaregiverId equals caregiver.CaregiverId
                                         select new PatientResponseDTO()
                                         {
                                             Username = user.Username,
                                             Id = patient.PatientId,
                                             MedicalRecord = patient.MedicalRecord,
                                             CaregiverName = caregiver.User.Username,
                                             UserId = user.UserId,
                                             CaregiverId = caregiver.CaregiverId
                                         };

                return await patientResponseDTOList.ToListAsync();
            }
        }

        public async Task<bool> UpdatePatient(Patient patient)
        {
            using(_context)
            {
                _context.Patients.Update(patient);
                var updated = await _context.SaveChangesAsync();

                return updated > 0;
            }
        }
    }
}
