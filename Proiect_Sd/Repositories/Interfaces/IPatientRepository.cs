﻿using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories.Interfaces
{
    public interface IPatientRepository
    {
        Task<List<PatientResponseDTO>> GetPatients();
        Task<PatientResponseDTO> GetPatientResponseById(int patientId);
        Task<Patient> GetPatientById(int pacientId);
        Task<Patient> CreatePatient(Patient patient);
        Task<bool> UpdatePatient(Patient patient);
        Task<bool> CheckIfPatientExists(int patientId);
        Task<bool> DeletePatient(Patient patient);
    }
}
