﻿using System.Threading.Tasks;

namespace Proiect_Sd
{
    public interface IGreeter
    {
        Task<HelloReply> SayHelloAsync(HelloRequest request);
    }
}
