﻿using Microsoft.EntityFrameworkCore;
using Proiect_Sd.Entities;

namespace Proiect_Sd.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Caregiver> Caregivers { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Medication> Medications { get; set; }
        public DbSet<MedicationPlan> MedicationPlans { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Patient>()
                .HasOne(p => p.User)
                .WithOne(u => u.Patient)
                .HasForeignKey<Patient>(p => p.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Doctor>()
                .HasOne(d => d.User)
                .WithOne(u => u.Doctor)
                .HasForeignKey<Doctor>(d => d.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Caregiver>()
                .HasOne(c => c.User)
                .WithOne(u => u.Caregiver)
                .HasForeignKey<Caregiver>(c => c.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Caregiver>()
                .HasMany(c => c.Patients)
                .WithOne(p => p.Caregiver)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<MedicationPlan>()
                .HasMany(mp => mp.Medications)
                .WithOne(m => m.MedicationPlan)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<Patient>()
                .HasMany(mp => mp.MedicationPlans)
                .WithOne(p => p.Patient)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
