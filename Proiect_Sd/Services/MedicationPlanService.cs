﻿using AutoMapper;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using Proiect_Sd.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services
{
    public class MedicationPlanService : IMedicationPlanService
    {
        private readonly IMedicationPlanRepository _medicationPlanRepository;
        private readonly IMapper _mapper;

        public MedicationPlanService(IMedicationPlanRepository medicationPlanRepository, IMapper mapper)
        {
            _medicationPlanRepository = medicationPlanRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfMedicationPlanExistsAsync(int medicationPlanId)
        {
            return await _medicationPlanRepository.CheckIfMedicationPlanExists(medicationPlanId);
        }

        public async Task<MedicationPlan> CreateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO)
        {
            var medicationPlan = _mapper.Map<MedicationPlan>(medicationPlanRequestDTO);
            return await _medicationPlanRepository.CreateMedicationPlan(medicationPlan);
        }

        public async Task<bool> DeleteMedicationPlanAsync(int medicationPlanId)
        {
            var medicationPlan = await GetMedicationPlanByIdAsync(medicationPlanId);

            return await _medicationPlanRepository.DeleteMedicationPlan(medicationPlan);
        }

        public async Task<MedicationPlan> GetMedicationPlanByIdAsync(int medicationPlanId)
        {
            return await _medicationPlanRepository.GetMedicationPlanById(medicationPlanId);
        }

        public async Task<MedicationPlanResponseDTO> GetMedicationPlanResponseByIdAsync(int medicationPlanId)
        {
            return await _medicationPlanRepository.GetMedicationPlanResponseById(medicationPlanId);
        }

        public async Task<List<MedicationPlanResponseDTO>> GetMedicationPlans()
        {
            return await _medicationPlanRepository.GetMedicationPlans();
        }

        public async Task<bool> UpdateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO, int medicationPlanId)
        {
            var medicationPlan = _mapper.Map<MedicationPlan>(medicationPlanRequestDTO);
            medicationPlan.PlanId = medicationPlanId;

            return await _medicationPlanRepository.UpdateMedicationPlan(medicationPlan);
        }
    }
}
