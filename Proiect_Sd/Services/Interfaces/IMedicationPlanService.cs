﻿using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public interface IMedicationPlanService
    {
        Task<List<MedicationPlanResponseDTO>> GetMedicationPlans();
        Task<MedicationPlanResponseDTO> GetMedicationPlanResponseByIdAsync(int medicationPlanId);
        Task<MedicationPlan> GetMedicationPlanByIdAsync(int medicationPlanId);
        Task<MedicationPlan> CreateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO);
        Task<bool> UpdateMedicationPlanAsync(MedicationPlanRequestDTO medicationPlanRequestDTO, int medicationPlanId);
        Task<bool> CheckIfMedicationPlanExistsAsync(int medicationPlanId);
        Task<bool> DeleteMedicationPlanAsync(int medicationPlanId);
    }
}
