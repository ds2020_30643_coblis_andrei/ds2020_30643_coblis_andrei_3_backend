﻿using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public interface IMedicationService
    {
        Task<List<MedicationResponseDTO>> GetMedications();
        Task<MedicationResponseDTO> GetMedicationResponseByIdAsync(int medicationId);
        Task<Medication> GetMedicationByIdAsync(int medicationId);
        Task<Medication> CreateMedicationAsync(MedicationRequestDTO medicationRequestDTO);
        Task<bool> UpdateMedicationAsync(MedicationRequestDTO medicationRequestDTO, int medicationId);
        Task<bool> CheckIfMedicationExistsAsync(int medicationId);
        Task<bool> DeleteMedicationAsync(int medicationId);
    }
}
