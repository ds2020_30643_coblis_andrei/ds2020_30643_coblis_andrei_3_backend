﻿using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public interface IDoctorService
    {
        Task<List<DoctorResponseDTO>> GetDoctors();
        Task<DoctorResponseDTO> GetDoctorResponseByIdAsync(int doctorId);
        Task<Doctor> GetDoctorByIdAsync(int doctorId);
        Task<Doctor> CreateDoctorAsync(DoctorRequestDTO doctorRequestDTO);
        Task<bool> UpdateDoctorAsync(DoctorRequestDTO doctorRequestDTO, int doctorId);
        Task<bool> CheckIfDoctorExistsAsync(int doctorId);
        Task<bool> DeleteDoctorAsync(int doctorId);
    }
}
