﻿using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public interface ICaregiverService
    {
        Task<List<CaregiverResponseDTO>> GetCaregivers();
        Task<CaregiverResponseDTO> GetCaregiverResponseByIdAsync(int caregiverId);
        Task<Caregiver> GetCaregiverByIdAsync(int caregiverId);
        Task<Caregiver> CreateCaregiverAsync(CaregiverRequestDTO caregiverRequestDTO);
        Task<bool> UpdateCaregiverAsync(CaregiverRequestDTO caregiverRequestDTO, int caregiverId);
        Task<bool> CheckIfCaregiverExistsAsync(int caregiverId);
        Task<bool> DeleteCaregiverAsync(int caregiverId);
    }
}
