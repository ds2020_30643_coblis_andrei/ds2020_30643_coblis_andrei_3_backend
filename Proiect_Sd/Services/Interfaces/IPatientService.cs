﻿using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public interface IPatientService
    {
        Task<List<PatientResponseDTO>> GetPatients();
        Task<PatientResponseDTO> GetPatientResponseByIdAsync(int patientId);
        Task<Patient> GetPatientByIdAsync(int patientId);
        Task<Patient> CreatePatientAsync(PatientRequestDTO patientRequestDTO);
        Task<bool> UpdatePatientAsync(PatientRequestDTO patientRequestDTO, int patientId);
        Task<bool> CheckIfPatientExistsAsync(int patientId);
        Task<bool> DeletePatientAsync(int patientId);
    }
}
