﻿using System.Threading.Tasks;

namespace Proiect_Sd
{
    public class GreeterServer : IGreeter
    {
        public Task<HelloReply> SayHelloAsync(HelloRequest request)
        {
            return Task.FromResult(new HelloReply
            {
                Message = "Hello " + request.Name
            });
        }
    }
}
