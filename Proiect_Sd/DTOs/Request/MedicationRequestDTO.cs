﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.DTOs.Request
{
    public class MedicationRequestDTO
    {
        [Required(ErrorMessage = "Please enter a medication plan id ")]
        public int MedicationPlanId { get; set; }

        [Required(ErrorMessage = "Please enter a name ")]
        [StringLength(30, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a dosage")]
        [StringLength(15, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string Dosage { get; set; }

        [Required(ErrorMessage = "Please enter a side effect")]
        [StringLength(200, ErrorMessage = "The {0} must be max {1} characters long.")]
        public string SideEffects { get; set; }
    }
}
