﻿using System;

namespace Proiect_Sd.DTOs.Response
{
    public class MedicationPlanResponseDTO
    {
        public int Id { get; set; }

        public int PatientId { get; set; }

        public string PatientName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
