#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
#EXPOSE 80
#EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Proiect_Sd/Proiect_Sd.csproj", "Proiect_Sd/"]
RUN dotnet restore "Proiect_Sd/Proiect_Sd.csproj"
COPY . .
WORKDIR "/src/Proiect_Sd"
RUN dotnet build "Proiect_Sd.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Proiect_Sd.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
#ENTRYPOINT ["dotnet", "Proiect_Sd.dll"]
CMD ASPNETCORE_URLS=http://*:$PORT dotnet Proiect_Sd.dll